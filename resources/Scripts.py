
TPT_EXPORT_SCRIPT = """
DEFINE JOB $job_name$
DESCRIPTION 'Exporta datos de $schema$.$object$ a un archivo formateado usando EXPORT'
(
     
	 DEFINE SCHEMA Define_$object$_Schema
	 DESCRIPTION 'Define el esquema del objeto a exportar'
	 (
$schema_object$
	 );
	 
	 DEFINE OPERATOR Consumer_File_Detail
     DESCRIPTION 'Definir un operador consumidor para almacenar datos recuperados en un archivo'
     TYPE DATACONNECTOR CONSUMER
     SCHEMA *
     ATTRIBUTES(
         VARCHAR FileName = '$path_file$',
         VARCHAR FORMAT = 'DELIMITED',
         VARCHAR OpenMode='Write',
         VARCHAR TextDelimiter='$delimiter$'
     );

     DEFINE OPERATOR Producer_Query_Detail
     TYPE EXPORT
     SCHEMA Define_$object$_Schema
     ATTRIBUTES(
		 VARCHAR Tdpid ='$host$',
         VARCHAR UserName ='$username$',
         VARCHAR UserPassword = '$password$',
         VARCHAR SelectStmt = 
         '$select_stmt$'
     );
	
     APPLY
     TO OPERATOR ( Consumer_File_Detail )
     SELECT * FROM OPERATOR( Producer_Query_Detail[$num_instances$] );
 );
"""

BTEQ_EXPORT_SCRIPT = """
.LOGON $host$/$username$,$password$;
.EXPORT DATA FILE = '$path_file$';
.SET RECORDMODE OFF;
.SET TITLEDASHES OFF;

$select_stmt$
 
.EXPORT RESET
.LOGOFF
.EXIT
"""


"""

TPTLOAD_DOCUMENT = "DEFINE JOB " + key.JOB_NAME + "\n" \
+ " DESCRIPTION 'Carga datos de un archivo formateado a " + key.OBJECT_TARGET + " usando LOAD'\n" \
+ " (\n" \
+ "\n" \
+ "\n" \
+ "     DEFINE OPERATOR Consumer_Table_Detail\n" \
+ "     DESCRIPTION ''\n" \
+ "     TYPE LOAD\n" \
+ "     SCHEMA *\n" \
+ "     ATTRIBUTES\n" \
+ "     (\n" \
+ "         VARCHAR TdPid = @tdpid,\n" \
+ "         VARCHAR UserName = @user_name,\n" \
+ "         VARCHAR UserPassword = @user_password,\n" \
+ "         VARCHAR TargetTable = '" + key.FULL_OBJECT_TARGET + "',\n" \
+ "         VARCHAR LogTable = '" + key.FULL_OBJECT_TARGET + "_LT',\n" \
+ "         VARCHAR DateForm = 'AnsiDate',\n" \
+ "         INTEGER MaxSessions=" + key.MAX_SESSIONS + ",\n" \
+ "         INTEGER MinSessions=" + key.MIN_SESSIONS + "\n" \
+ "     );\n" \
+ "\n" \
+ "     DEFINE SCHEMA Define_" + key.OBJECT_TARGET + "_Schema\n" \
+ "     DESCRIPTION 'Definir un esquema para describir la estructura de la tabla a cargar'\n" \
+ "     (\n" \
+ key.COLUMNS_SCHEMA_TARGET + "\n" \
+ "     );\n" \
+ "\n" \
+ "     DEFINE OPERATOR Producer_File_Detail\n" \
+ "     TYPE DATACONNECTOR PRODUCER\n" \
+ "     SCHEMA Define_" + key.OBJECT_TARGET + "_Schema\n" \
+ "     ATTRIBUTES\n" \
+ "     (\n" \
+ "         VARCHAR FileName = @file_out,\n" \
+ "         VARCHAR TextDelimiter = '" + key.DELIMITER + "',\n" \
+ "         VARCHAR Format = 'Delimited',\n" \
+ "         VARCHAR OpenMode = 'Read',\n" \
+ "         VARCHAR IndicatorMode = 'N',\n" \
+ "         VARCHAR DateForm = 'AnsiDate',\n" \
+ "         INTEGER SkipRows = " + key.SKIP_ROWS + ",\n" \
+ "         VARCHAR SkipRowsEveryFile = 'N'\n" \
+ "     );\n" \
+ "\n" \
+ "     APPLY\n" \
+ "     (\n" \
+ "         'INSERT INTO " + key.FULL_OBJECT_TARGET + "\n" \
+ "         (\n" \
+ key.COLUMNS_SELECT_TARGET + "\n" \
+ "         )\n" \
+ "         VALUES\n" \
+ "         (\n" \
+ key.COLUMNS_VALUES_TARGET + "\n" \
+ "         );'\n" \
+ "      )\n" \
+ "     TO OPERATOR ( Consumer_Table_Detail )\n" \
+ "     SELECT * FROM OPERATOR( Producer_File_Detail );\n" \
+ "\n" \
+ " );"

"""









