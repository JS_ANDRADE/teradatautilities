def parserColumnTeradataToTPT(type, max_length, decimal_total_digits, decimal_fractional_digits):
    result = ""

    column_type = ""
    column_length = ""

    dict_type = {
        'A1':'ARRAY',
        'AN':'MULTI-DIMENSIONALARRAY',
        'AT':'TIME',
        'BF':'BYTE',
        'BO':'BLOB',
        'BV':'VARBYTE',
        'CF':'CHAR',
        'CO':'CLOB',
        'CV':'VARCHAR',
        'D':'DECIMAL',
        'DA':'DATE',
        'DH':'INTERVALDAYTOHOUR',
        'DM':'INTERVALDAYTOMINUTE',
        'DS':'INTERVALDAYTOSECOND',
        'DY':'INTERVALDAY',
        'F':'FLOAT',
        'HM':'INTERVALHOURTOMINUTE',
        'HS':'INTERVALHOURTOSECOND',
        'HR':'INTERVALHOUR',
        'I':'INTEGER',
        'I1':'BYTEINT',
        'I2':'SMALLINT',
        'I8':'BIGINT',
        'JN':'JSON',
        'MI':'INTERVALMINUTE',
        'MO':'INTERVALMONTH',
        'MS':'INTERVALMINUTETOSECOND',
        'N':'NUMBER',
        'PD':'PERIOD(DATE)',
        'PM':'PERIOD(TIMESTAMPWITHTIMEZONE)',
        'PS':'PERIOD(TIMESTAMP)',
        'PT':'PERIOD(TIME)',
        'PZ':'PERIOD(TIMEWITHTIMEZONE)',
        'SC':'INTERVALSECOND',
        'SZ':'TIMESTAMPWITHTIMEZONE',
        'TS':'CHAR',
        'TZ':'TIMEWITHTIMEZONE',
        'UT':'UDTType',
        'XM':'XML',
        'YM':'INTERVALYEARTOMONTH',
        'YR':'INTERVALYEAR',
    }

    column_type = dict_type.get(type)

    if type in ['CF', 'CV', 'TS']:
        column_length = normalizeFloat(max_length)
    elif type in ['T2']:
        column_length = normalizeFloat(decimal_total_digits)
    elif type in ['D']:
        column_length = normalizeFloat(decimal_fractional_digits) + "," + normalizeFloat(decimal_fractional_digits)
    else:
        column_length = None

    if column_type != None:
        if column_length != None:

            result=column_type + "(" + column_length + ")"
        else:
            result = column_type

    return result

def parserColumnTeradata(type, max_length, decimal_total_digits, decimal_fractional_digits):
    result = ""

    column_type = ""
    column_length = ""

    dict_type = {
        'A1':'ARRAY',
        'AN':'MULTI-DIMENSIONALARRAY',
        'AT':'TIME',
        'BF':'BYTE',
        'BO':'BLOB',
        'BV':'VARBYTE',
        'CF':'CHAR',
        'CO':'CLOB',
        'CV':'VARCHAR',
        'D':'DECIMAL',
        'DA':'DATE',
        'DH':'INTERVALDAYTOHOUR',
        'DM':'INTERVALDAYTOMINUTE',
        'DS':'INTERVALDAYTOSECOND',
        'DY':'INTERVALDAY',
        'F':'FLOAT',
        'HM':'INTERVALHOURTOMINUTE',
        'HS':'INTERVALHOURTOSECOND',
        'HR':'INTERVALHOUR',
        'I':'INTEGER',
        'I1':'BYTEINT',
        'I2':'SMALLINT',
        'I8':'BIGINT',
        'JN':'JSON',
        'MI':'INTERVALMINUTE',
        'MO':'INTERVALMONTH',
        'MS':'INTERVALMINUTETOSECOND',
        'N':'NUMBER',
        'PD':'PERIOD(DATE)',
        'PM':'PERIOD(TIMESTAMPWITHTIMEZONE)',
        'PS':'PERIOD(TIMESTAMP)',
        'PT':'PERIOD(TIME)',
        'PZ':'PERIOD(TIMEWITHTIMEZONE)',
        'SC':'INTERVALSECOND',
        'SZ':'TIMESTAMPWITHTIMEZONE',
        'TS':'TIMESTAMP',
        'TZ':'TIMEWITHTIMEZONE',
        'UT':'UDTType',
        'XM':'XML',
        'YM':'INTERVALYEARTOMONTH',
        'YR':'INTERVALYEAR',
    }

    column_type = dict_type.get(type)

    if type in ['CF', 'CV', 'TS']:
        column_length = normalizeFloat(max_length)
    elif type in ['T2']:
        column_length = normalizeFloat(decimal_total_digits)
    elif type in ['D']:
        column_length = normalizeFloat(decimal_fractional_digits) + "," + normalizeFloat(decimal_fractional_digits)
    else:
        column_length = None

    if column_type != None:
        if column_length != None:

            result=column_type + "(" + column_length + ")"
        else:
            result = column_type

    return result


def normalizeFloat(num):
    return num.rstrip('0').rstrip('.')