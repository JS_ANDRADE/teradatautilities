import os
import platform
import subprocess

import utilities_js.operations.document.File as file_util
import utilities_js.operations.format.List as format_list
import resources.Scripts as scripts
import utilities_js.connections.server.operation.UnixOperations as unix



class TptOperations():

    _action = None

    COMMAND_TPT = "tbuild"
    ARG_FILE_SCRIPT = "-f"

    def __init__(self, action):
        self._action = action

    # OPERACIONES GENERALES

    ## Creacion del script
    def createScript(self, type, data):

        if type == "EXPORT":
            self.createScriptExport(data)
        else:
            print("TIPO DE OPERACION NO IMPLEMENTADA")

    def execScript(self, data):

        system = platform.system()
        if data['$location$'] == "SERVER":

            file = os.path.join(data['$path_script$'], data['$name_script$']);
            self._action.appendLogView("Inicio de la ejecucion del script {0}.".format(file))

            command = "{0} {1} {2}".format(self.COMMAND_TPT, self.ARG_FILE_SCRIPT, file)

            result = unix.execCommand(data['$connection_server$'], command)
            self._action.appendLogView(result)
            result = unix.execCommand(data['$connection_server$'], "rm " + file)
            self._action.appendLogView(result)

        elif data['$location$'] == "LOCAL":
            file = data['$name_script$'];
            self._action.appendLogView("Inicio de la ejecucion del script {0}.".format(file))

            command = "{0} {1} {2}".format(self.COMMAND_TPT, self.ARG_FILE_SCRIPT, file)

            process = subprocess.Popen(command,
                                shell=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)

            for line in process.stdout:
                self._action.appendLogView(line.decode("utf-8"))

            errcode = process.returncode

            os.remove(file)

        self._action.appendLogView("El script se ha ejecutado correctamente.")

        #return errcode

    ## Creacion del script de export
    def createScriptExport(self, data):

        if data['$location$'] == "SERVER":
            file = data['$name_script$'];
        elif data['$location$'] == "LOCAL":
            file = os.path.join(data['$path_script$'], data['$name_script$']);

        self._action.appendLogView("Inicio de la generacion del script {0}.".format(file))

        # Agregamos nuevos campos al diccionario
        data['$schema_object$'] = self.formatSchemaTpt(data)
        data['$select_stmt$'] = self.formatSelect(data)

        file_util.writeContent(file, scripts.TPT_EXPORT_SCRIPT)

        self.replaceValuesFile(file, data)



        if data['$location$'] == "SERVER":
            file_util.convertCRLFToLF(file)

            self._action.appendLogView("Se ha convertido el script a formato LINUX/UNIX.")

            local_file = data['$name_script$']
            path_remote = data['$path_script$']

            unix.putFile(data['$connection_server$'], local_file, path_remote + "/" + local_file)

            self._action.appendLogView("Se ha subido el script al servidor de {0} a {1} mediante sftp.".format(local_file, path_remote + "/" + local_file))

            os.remove(local_file)

            self._action.appendLogView("Se ha eliminado el script local {0}.".format(local_file))

        elif data['$location$'] == "LOCAL":
            if platform.system().upper() == "WINDOWS":
                file_util.convertLFToCRLF(file)
                self._action.appendLogView("Se ha convertido el script a formato WINDOWS.")
            elif platform.system().upper() == "LINUX":
                file_util.convertCRLFToLF(file)
                self._action.appendLogView("Se ha convertido el script a formato LINUX/UNIX.")

        self._action.appendLogView("El script se ha generado correctamente.")


    # OPERACIONES

    ## Remplazar las claves de un fichero por sus valores
    def replaceValuesFile(self, file, dict):
        for key, value in dict.items():
            file_util.replaceText(file, key, value)


    # FORMATEOS

    ## Formateo para el esquema del tpt a apartir de los campos
    def formatSchemaTpt(self, data):
        fields = data['$fields_object$']
        list = []

        for i in range(len(fields)):
            list.append(fields[i][0] + " " + fields[i][1])

        return format_list.formatSeparateList(list, "\t\t$ELEMENT$", ",\n")


    ## Formateo para la select
    def formatSelect(self, data):
        fields = data['$fields_object$']
        list = []

        for i in range(len(fields)):
            if fields[i][2] == None:
                list.append(fields[i][0])
            else:
                list.append(fields[i][2])

        fields_select = format_list.formatSeparateList(list, "\t\t $ELEMENT$", ",\n")

        return "SELECT\n{0}\n\t\t FROM {1}.{2};".format(fields_select, data['$schema$'], data['$object$'])

