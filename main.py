# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets

import sys
sys.path.append("..")

from widgets.MainWindow import MainWindow

#Metodo para que los mensajes de las excpeciones
def except_hook(cls, exception, traceback):
    sys.__excepthook__(cls, exception, traceback)


if __name__ == "__main__":

    sys.excepthook = except_hook

    app = QtWidgets.QApplication([])
    window = MainWindow()
    window.show()
    app.exec_()

