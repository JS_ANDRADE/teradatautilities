# -*- coding: utf-8 -*-

# Form implementation generated from reading views file 'Main.views'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btLoad = QtWidgets.QPushButton(self.centralwidget)
        self.btLoad.setGeometry(QtCore.QRect(250, 400, 300, 125))
        self.btLoad.setObjectName("btLoad")
        self.btExportLoad = QtWidgets.QPushButton(self.centralwidget)
        self.btExportLoad.setGeometry(QtCore.QRect(250, 50, 300, 125))
        self.btExportLoad.setObjectName("btExportLoad")
        self.btExport = QtWidgets.QPushButton(self.centralwidget)
        self.btExport.setGeometry(QtCore.QRect(250, 225, 300, 124))
        self.btExport.setObjectName("btExport")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 31))
        self.menubar.setObjectName("menubar")
        self.menuOpciones = QtWidgets.QMenu(self.menubar)
        self.menuOpciones.setObjectName("menuOpciones")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionConnectionsBBDD = QtWidgets.QAction(MainWindow)
        self.actionConnectionsBBDD.setObjectName("actionConnectionsBBDD")
        self.actionFichero_Conexiones = QtWidgets.QAction(MainWindow)
        self.actionFichero_Conexiones.setObjectName("actionFichero_Conexiones")
        self.actionConnectionsServers = QtWidgets.QAction(MainWindow)
        self.actionConnectionsServers.setObjectName("actionConnectionsServers")
        self.menubar.addAction(self.menuOpciones.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btLoad.setText(_translate("MainWindow", "LOAD"))
        self.btExportLoad.setText(_translate("MainWindow", "EXPORT/LOAD"))
        self.btExport.setText(_translate("MainWindow", "EXPORT"))
        self.menuOpciones.setTitle(_translate("MainWindow", "Opciones"))
        self.actionConnectionsBBDD.setText(_translate("MainWindow", "Conexiones BBD"))
        self.actionFichero_Conexiones.setText(_translate("MainWindow", "Fichero Conexiones"))
        self.actionConnectionsServers.setText(_translate("MainWindow", "Conexiones Servidores"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

