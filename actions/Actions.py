import json
import os
import subprocess
import traceback
import threading

import resources.Utils as utils

import utilities_js.connections.server.operation.UnixOperations as unix
import utilities_js.connections.database.operation.TeradataOperations as teradata
from  utilities_js.connections.model.DatabaseConnectionModel import DatabaseConnectionModel

from operations.TptOperations import TptOperations
from operations.BteqOperations import BteqOperations

from utilities_js.connections.model.ServerConnectionModel import ServerConnectionModel
from utilities_js.exceptions.ErrorConnections import DatabaseError, SSHError


class Actions():

    _view = None

    _tpt_operations = None
    _bteq_operations = None


    def __init__(self, view):

        self._view = view

        self._tpt_operations = TptOperations(self)
        self._bteq_operations = BteqOperations(self)


    # CONEXIONES

    ## Metodo para obtener el nombre de las conexiones disponibles
    def getNameConnections(self):
        return self.getNameConnectionsTeradata()

    ## Metodo para obtener el nombre de  las conexion de bbdd teradata
    def getNameConnectionsTeradata(self):
        dict = {}
        with open('connections.json') as file:
            data = json.load(file)

            for c in data['connections']:
                for db in c['databases']:
                    if[db['type'] == "TERADATA"]:
                        dict[db['id']] = db['name']

        return  dict

    ## Metodo para obtener el nombre de las conexiones de servidores disponibles
    def getNameConnectionsServer(self):
        return self.getNameConnectionsUnix()

    ## Metodo para obtener el nombre de  las conexion de servidores unix
    def getNameConnectionsUnix(self):
        dict = {}
        with open('connections.json') as file:
            data = json.load(file)

            for c in data['connections']:
                for db in c['servers']:
                    if [db['type'] == "UNIX"]:
                        dict[db['id']] = db['name']
        return dict

    ## Metodo para obtener el nombre de conexion por el nombre
    def getConnectionByName(self, name_connection):

        connection = DatabaseConnectionModel();

        with open('connections.json') as file:
            data = json.load(file)

            for c in data['connections']:
                for db in c['databases']:
                    if db['name'] == name_connection:
                        connection.host = db['host']
                        connection.user = db['user']
                        connection.password = db['password']
                        connection.port = db['port']
                        connection.driver = db['driver']

        return connection

    ## Metodo para obtener el nombre de conexion por el nombre
    def getConnectionById(self, id_connection):

        connection = DatabaseConnectionModel();

        with open('connections.json') as file:
            data = json.load(file)

            for c in data['connections']:
                for db in c['databases']:
                    if db['id'] == id_connection:
                        connection.host = db['host']
                        connection.user = db['user']
                        connection.password = db['password']
                        connection.port = db['port']
                        connection.driver = db['driver']

        return connection

    def getConnectionServerById(self, id_connection):

        connection = ServerConnectionModel()

        with open('connections.json') as file:
            data = json.load(file)

            for c in data['connections']:
                for db in c['servers']:
                    if db['id'] == id_connection:
                        connection.host = db['host']
                        connection.user = db['user']
                        connection.password = db['password']
                        connection.port = db['port']

        return connection



    # OPERACIONS BBDD

    ## MEtodo para obtener los esquemas de una conexion
    def getSchemasOfConnection(self, id_connection):
        try:
            connection = self.getConnectionById(id_connection)

            return teradata.getSchemas(connection)
        except DatabaseError as te:
            self._view.showError(str(te))
            return None

    ## Metodo para obtener los objetos de un esquema
    def getObjectsOfSchema(self, id_connection, schema):
        try:
            connection = self.getConnectionById(id_connection)
            args = [schema]

            return teradata.getObjectsOfSchema(connection, args)
        except DatabaseError as te:
            self._view.showError(str(te))
            return None

    # Metodo para obtener los campos de un objeto
    def getFieldsObject(self, id_connection, schema, object):
        try:
            connection = self.getConnectionById(id_connection)

            args = [schema, object]

            return teradata.helpColumnsObject(connection, args)
        except DatabaseError as te:
            self._view.showError(str(te))
            return None

    # Metodo para obtener los campos formateados de un objeto (SELECT)
    def getFieldsSelectObject(self, id_connection, schema, object, app):
        try:
            fields_object = self.getFieldsObject(id_connection, schema, object)

            data_fields_object = []

            for i in range(len(fields_object)):
                data_fields_object.append([])

                name = fields_object[i][0]
                type = fields_object[i][1]
                max_lengh = fields_object[i][2]
                decimal_total_digits = fields_object[i][3]
                decimal_fractional_digits = fields_object[i][4]

                data_fields_object[i].append(name)
                if app == "TPT":
                    data_fields_object[i].append(
                        self.parserTypeFieldTeradataToTpt(type, max_lengh, decimal_total_digits,
                                                               decimal_fractional_digits))
                else:
                    data_fields_object[i].append(
                        self.parserTypeFieldTeradata(type, max_lengh, decimal_total_digits,
                                                          decimal_fractional_digits))
                data_fields_object[i].append(None)

            return data_fields_object
        except DatabaseError as te:
            self._view.showError(str(te))
            return None
    # OPERACIONES UNIX

    # Metodo para ejecutar los scripts
    def exec(self, action, data):

        app = data['$type$']
        subtypeapp = data['$subtype$']
        try:
            if app == "TPT":
                if action == "EXPORT":
                    self._tpt_operations.createScript(action, data)
                    self._tpt_operations.execScript(data)
                else:
                    self._view.showWarning(
                        "Tipo de accion {0} no implementada para la aplicacion {1}".format(action, app))
                    return  -1
            elif app == "BTEQ":
                if action == "EXPORT":
                    self._bteq_operations.createScript(action, data)
                    self._bteq_operations.execScript(data)
                else:
                    self._view.showWarning(
                        "Tipo de accion {0} no implementada para la aplicacion {1}".format(action, app))
                    return -1
            else:
                self._view.showWarning("Tipo de aplicacion {0} no implementada".format(app))
                return -1
        except SSHError as se:
            self._view.appendLog(str(se))
            return 2

        return 0


    # FIHCEROS OPERACIONES

    # Metodo para generar el codigo
    def generateCode(self, action, data):

        app = data['$type$']
        subtypeapp = data['$subtype$']
        try:
            if app == "TPT":
                if action != "EXPORT":
                    self._view.showWarning("Tipo de accion {0} no implementada para la aplicacion {1}".format(action, app))
                    return -1
                else:
                    self._tpt_operations.createScript(action, data)
            elif app == "BTEQ":
                if action != "EXPORT":
                    self._view.showWarning("Tipo de accion {0} no implementada para la aplicacion {1}".format(action, app))
                    return -1
                else:
                    self._bteq_operations.createScript(action, data)
            else:
                self._view.showWarning("Tipo de aplicacion {0} no implementada".format(app))
                return -1

        except FileNotFoundError as fnfe:
            self._view.appendLog("ERROR: " + str(fnfe))
            traceback.print_exc()
            return 1
        except SSHError as se:
            self._view.appendLog("ERROR: " + str(se))
            return 2
        return 0

    # PARSERS
    # Metodo para cambiar el tipo de datos de teradata a tpt
    def parserTypeFieldTeradataToTpt(self, type, max_length, decimal_total_digits, decimal_fractional_digits):
        return utils.parserColumnTeradataToTPT(type, max_length, decimal_total_digits, decimal_fractional_digits)

    # Metodo para obtener los campos en formato de teradata
    def parserTypeFieldTeradata(self, type, max_length, decimal_total_digits, decimal_fractional_digits):
        return utils.parserColumnTeradata(type, max_length, decimal_total_digits, decimal_fractional_digits)


    # METODOS VISTA

    def appendLogView(self, msg):
        self._view.appendLog(msg)

