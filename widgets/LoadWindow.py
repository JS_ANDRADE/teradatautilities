import threading

from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtWidgets import QDialog, QMessageBox
from ui.LoadUi import *
from actions.Actions import Actions

from utilities_js.ui.widgets.EditTextDialog import EditTextDialog
from utilities_js.ui.widgets.EditLineDialog import EditLineDialog
from utilities_js.ui.widgets.SelectFieldsDialog import SelectFieldsDialog


class LoadWindow(QtWidgets.QDialog, Ui_Dialog):

    TYPE_ACTION="LOAD"

    _actions = None

    _id_connections = None
    _id_connections_server = None

    _data_insert = None
    _data_insert_statement = None

    def __init__(self, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        self.setupUi(self)
        
        self.initComponents()
        
    #Metodo para precargar la configuracion de la ventana
    def preloadData(self):
        #self.lePathFile.setText("/sastest/SAS/PIC/JS/pruebas/prueba.txt")
        self.lePathFile.setText("C:\\ficheros\\pruebas_teradata_tools\\prueba.txt")

        self.cbLocation.setEnabled(True)
        self.cbLocation.setCurrentIndex(0)

        self.cbTypeLoad.setEnabled(True)
        self.cbTypeLoad.setCurrentIndex(0)

        self.cbConnectionTarget.setEnabled(True)
        self.cbConnectionTarget.setCurrentIndex(1)

        self.cbSchemaTarget.setEnabled(True)
        #self.cbSchemaTarget.setEditText("PIC_V")
        self.cbSchemaTarget.setCurrentIndex(94)

        self.cbObjectTarget.setEnabled(True)
        #self.cbObjectTarget.setEditText("V_AX_PIC_CARGO_DESCUENTO_E")
        self.cbObjectTarget.setCurrentIndex(71);

        self.leJobName.setText("PRUEBA_EXPORT_JS")
        self.leDelimiter.setText("|")
        self.leNumSessions.setText("4")
        self.leNumInstances.setText("1")



    #Metodo para inicializar los componentes de la ventan
    def initComponents(self):
        self._actions = Actions(self)

        self.initWidgets()
        self.connectSignals()
        self.setDefaultComponentes()

        self.preloadData()

    # Metodo que deja los componentes como si se abriera por primera vez la pantalla
    def setDefaultComponentes(self):

        self.cbLocation.setCurrentIndex(-1)
        self.cbConnectionServer.setEnabled(False)
        self.cbTypeLoad.setCurrentIndex(-1)
        self.cbSubTypeLoad.setEnabled(False)
        self.lePathFile.setText(None)
        self.setCbConnections()
        self.cbSchemaTarget.setEnabled(False)
        self.cbObjectTarget.setEnabled(False)
        self.leJobName.setText(None)
        self.btEditInsert.setEnabled(False)
        self.btEditStatement.setEnabled(False)
        self.checkStatementPersonalized.setChecked(False)
        self.checkStatementPersonalized.setEnabled(False)
        self.pteLog.setPlainText(None)

    # Inicalizar widgets
    def initWidgets(self):
        self.cbSchemaTarget.completer().setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        self.cbObjectTarget.completer().setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        self.cbConnectionTarget.completer().setCompletionMode(QtWidgets.QCompleter.PopupCompletion)

    # Conectar slots
    def connectSignals(self):

        self.cbLocation.currentIndexChanged.connect(self.changedLocation)

        self.cbTypeLoad.currentIndexChanged.connect(self.changedTypeLoad)

        self.cbConnectionTarget.currentIndexChanged.connect(self.changedConnectionTarget)
        self.cbSchemaTarget.currentIndexChanged.connect(self.changedSchemaTarget)
        self.cbObjectTarget.currentIndexChanged.connect(self.changedObjectTarget)

        self.btEditInsert.clicked.connect(self.actionEditInsert)
        self.btEditStatement.clicked.connect(self.actionEditStatement)

        self.checkStatementPersonalized.stateChanged.connect(self.changedCheckStatementPersonalized)

        self.btClean.clicked.connect(self.actionReset)
        self.btGenerateCode.clicked.connect(self.actionGenerateCode)
        self.btExec.clicked.connect(self.actionExec)

    # METODOS CONEXIONES SLOTS

    ## INICIALIZAR COMBOS

    ## LOCALIZACION
    def changedLocation(self):

        location = self.cbLocation.currentText()

        if ( location == "SERVER"):
            self.cbConnectionServer.setEnabled(True)
            self.setCbConnectionServer()
        else:
            self.cbConnectionServer.setEnabled(False)
            self.cbConnectionServer.clear()

    def setCbConnectionServer(self):

        dict_connections = self._actions.getNameConnectionsServer()
        name_connections = [*dict_connections.values()]
        self._id_connections_server = [*dict_connections]

        name_connections.insert(0, "-- ELIGE CONEXION --")
        self._id_connections_server.insert(0, 0)

        self.cbConnectionServer.addItems(name_connections)

    ## TIPO DE EXPORTACION
    def changedTypeLoad(self):

        type_load = self.cbTypeLoad.currentText()

        if self.cbConnectionTarget.currentIndex() > 0:
            id_connection = self._id_connections[self.cbConnectionTarget.currentIndex()]
            schema = self.cbSchemaTarget.currentText()
            object = self.cbObjectTarget.currentText()
            self._data_insert = self._actions.getFieldsSelectObject(id_connection, schema, object, type_load)


        if ( type_load == "TPT"):
            self.cbSubTypeLoad.setEnabled(True)
            self.setCbSubTypeLoad(type_load)
        else:
            self.cbSubTypeLoad.setEnabled(False)
            self.cbSubTypeLoad.clear()



    def setCbSubTypeLoad(self, type):

        if type == "TPT":
            self.cbSubTypeLoad.addItems(['EXPORT'])


    ### COMBO CONEXIONES

    def setCbConnections(self):

        dict_connections = self._actions.getNameConnections()
        name_connections = [*dict_connections.values()]
        self._id_connections= [*dict_connections]


        name_connections.insert(0, "-- ELIGE CONEXION --")
        self._id_connections.insert(0, 0)

        self.cbConnectionTarget.clear()
        self.cbConnectionTarget.addItems(name_connections)
        self.cbConnectionTarget.setCurrentIndex(0)

    ### CAMBIO COMBO CONEXIONES
    def changedConnectionTarget(self):
        id_connection = self._id_connections[self.cbConnectionTarget.currentIndex()]

        if (id_connection != None and self.cbConnectionTarget.currentIndex() > 0):
            schemas = self._actions.getSchemasOfConnection(id_connection)

            if schemas != None:
                schemas.insert(0, "-- ELIGE ESQUEMA --")

                self.cbSchemaTarget.clear()
                self.cbSchemaTarget.addItems(schemas)
                self.cbSchemaTarget.setEnabled(True)
                self.cbSchemaTarget.setCurrentIndex(0)
            else:
                self.cbConnectionTarget.setCurrentIndex(0)

        elif (self.cbConnectionTarget.currentIndex() == 0):
            self.cbSchemaTarget.clear()
            self.cbSchemaTarget.setEnabled(False)

            self.cbObjectTarget.clear()
            self.cbObjectTarget.setEnabled(False)
        else:
            self.cbSchemaTarget.clear()
            self.cbSchemaTarget.setEnabled(False)

            self.cbObjectTarget.clear()
            self.cbObjectTarget.setEnabled(False)

    ### CAMBIO COMBO ESQUEMAS
    def changedSchemaTarget(self):
        id_connection = self._id_connections[self.cbConnectionTarget.currentIndex()]
        schema = self.cbSchemaTarget.currentText().strip()

        if (id_connection != None and self.cbConnectionTarget.currentIndex() > 0
                and schema != None and self.cbSchemaTarget.currentIndex() > 0):
            objects = self._actions.getObjectsOfSchema(id_connection, schema)

            if objects != None:
                objects.insert(0, "-- ELIGE OBJETO --")

                self.cbObjectTarget.clear()
                self.cbObjectTarget.addItems(objects)
                self.cbObjectTarget.setEnabled(True)
                self.cbObjectTarget.setCurrentIndex(0)
            else:
                self.cbSchemaTarget.setCurrentIndex(0)

        else:
            self.cbObjectTarget.setEnabled(False)
            self.cbObjectTarget.clear()

    ### CAMBIO COMBO OBEJTOS
    def changedObjectTarget(self):
        self._data_insert = None
        self._data_where = None
        self._data_select_statement = None

        if self._data_insert == None and self.cbObjectTarget.currentIndex()>0:
            id_connection = self._id_connections[self.cbConnectionTarget.currentIndex()]
            schema = self.cbSchemaTarget.currentText()
            object = self.cbObjectTarget.currentText()
            type_export = self.cbTypeLoad.currentText()

            self._data_insert = self._actions.getFieldsSelectObject(id_connection, schema, object, type_export)

        if  self.cbObjectTarget.currentIndex()>0:
            self.checkStatementPersonalized.setEnabled(True)
            if self.checkStatementPersonalized.isChecked():
                self.btEditStatement.setEnabled(True)
            else:
                self.btEditInsert.setEnabled(True)
        else:
            self.checkStatementPersonalized.setEnabled(False)
            self.checkStatementPersonalized.setChecked(False)

            self.btEditStatement.setEnabled(False)

            self.btEditInsert.setEnabled(False)

    ## ACCIONES

    ### Accion del boton exec
    def actionExec(self):

        if (self.validateData()):
            app = self.cbTypeLoad.currentText()
            type_app = self.cbSubTypeLoad.currentText()
            data = self.getData()

            data['$path_script$'] = "./"

            self.clearLog()

            self.appendLog("----------------------------------------------------------------------------------")
            self.appendLog("INICIO DE LA EJECUCION DEL SCRIPT " + app)
            self.appendLog("----------------------------------------------------------------------------------")

            cod_return = self._actions.exec(self.TYPE_ACTION, data)

            if cod_return == 0:
                self.appendLog("----------------------------------------------------------------------------------")
                self.appendLog("FIN DE LA EJECUCION DEL SCRIPT " + app + " OK")
                self.appendLog("----------------------------------------------------------------------------------")
            else:
                self.appendLog("----------------------------------------------------------------------------------")
                self.appendLog("FIN DE LA EJECUCION DEL SCRIPT " + app + " KO - ERROR " + str(cod_return))
                self.appendLog("----------------------------------------------------------------------------------")

    ### Accion del boton reset
    def actionReset(self):
        self.setDefaultComponentes()

    ### Accion del boton generate code
    def actionGenerateCode(self):

        if (self.validateData()):

            dialog = EditLineDialog()
            dialog.setTitle("Seleccione la ruta del script:")
            if dialog.exec_() == QDialog.Accepted:
                if (dialog.getText() != None and dialog.getText() != ""):
                    app = self.cbTypeLoad.currentText()
                    data = self.getData()
                    data['$path_script$'] = dialog.getText()

                    self.clearLog()

                    self.appendLog("----------------------------------------------------------------------------------")
                    self.appendLog("INICIO DE LA GENERACION DEL SCRIPT " + app)
                    self.appendLog("----------------------------------------------------------------------------------")

                    cod_return = self._actions.generateCode(self.TYPE_ACTION, data)

                    if cod_return == 0:
                        self.appendLog("----------------------------------------------------------------------------------")
                        self.appendLog("FIN DE LA GENERACION DEL SCRIPT " + app + " OK")
                        self.appendLog("----------------------------------------------------------------------------------")
                    else:
                        self.appendLog("----------------------------------------------------------------------------------")
                        self.appendLog("FIN DE LA GENERACION DEL SCRIPT " + app + " KO - ERROR " + str(cod_return))
                        self.appendLog("----------------------------------------------------------------------------------")

    ### Accion del boton edit select
    def actionEditInsert(self):
        name_object = self.cbSchemaTarget.currentText() + "." + self.cbObjectTarget.currentText()

        dialog = SelectFieldsDialog()

        dialog.setNameObject(name_object)
        dialog.setFieldsObject(self._data_insert)

        if dialog.exec_() == QDialog.Accepted:
            self._data_insert = dialog.getData()


    ### Accion del boton edit statement
    def actionEditStatement(self):

        dialog = EditTextDialog()

        if ( self._data_insert_statement != None and self._data_insert_statement != "" ):
            dialog.setText(self._data_insert_statement)

        if dialog.exec_() == QDialog.Accepted:
            if (dialog.getText() != None and dialog.getText() != ""):
                self._data_insert_statement = dialog.getText()
                self.updateSelectStament()

        return True

    ## Accion al clickar el check de personalzar sentencia
    def changedCheckStatementPersonalized(self):

        if self.checkStatementPersonalized.isChecked():
            self.btEditStatement.setEnabled(True)

            self.btEditInsert.setEnabled(False)
            self.btEditWhere.setEnabled(False)
        else:
            self.btEditStatement.setEnabled(False)

            self.btEditInsert.setEnabled(True)
            self.btEditWhere.setEnabled(True)

    #Metodo para actualizar la sentencia select
    def updateSelectStament(self):
        str = "SELECT \n"

        str= str + "* \n"

        str = str + "FROM " + self._object + "\n"

        if ( self._where_object != None ):
            str =  str + "WHERE " + self._where_object + "\n"

        self._data_insert_statement = str

    # Metodo para obtener los datos de la interface
    def getData(self):

        # Datos
        id_connection_db = self._id_connections[self.cbConnectionTarget.currentIndex()]
        connection_db_data = self._actions.getConnectionById(id_connection_db)
        location = self.cbLocation.currentText().strip()
        type_export = self.cbTypeLoad.currentText().strip()
        sub_type_export = self.cbSubTypeLoad.currentText().strip()
        path_file = self.lePathFile.text().strip()
        connection = self.cbConnectionTarget.currentText().strip()
        schema = self.cbSchemaTarget.currentText().strip()
        object = self.cbObjectTarget.currentText().strip()
        job_name = self.leJobName.text().strip()
        delimiter = self.leDelimiter.text().strip()
        num_sessions = self.leNumSessions.text().strip()
        num_instances = self.leNumInstances.text().strip()
        fields_object = self._data_insert
        host = connection_db_data.host
        username = connection_db_data.user
        password = connection_db_data.password
        extension_script = None
        if (type_export.upper() == "TPT" ):
            extension_script = "tpt"
        elif (type_export.upper() == "BTEQ"):
            extension_script = "btq"
        name_script =  type_export.lower() + "_script_" + self.TYPE_ACTION.lower() + "." + extension_script
        id_connection_server = -1
        connection_server_data = None
        if location == "SERVER":
            id_connection_server = self._id_connections_server[self.cbConnectionServer.currentIndex()]
            connection_server_data = self._actions.getConnectionServerById(id_connection_server)


        # Asignacion de datos al diccionario

        data = {
            '$location$' : location,
            '$type$' : type_export,
            '$subtype$' : sub_type_export,
            '$path_file$' : path_file,
            '$id_connection_db$' : id_connection_db,
            '$connection$' : connection,
            '$schema$' : schema,
            '$object$' : object,
            '$job_name$' : job_name,
            '$delimiter$' : delimiter,
            '$num_sessions$' : num_sessions,
            '$num_instances$' : num_instances,
            '$fields_object$' : fields_object,
            '$host$' : host,
            '$username$' : username,
            '$password$' : password,
            '$name_script$' : name_script,
            '$connection_db$' : connection_db_data,
            '$connection_server$' : connection_server_data
        }

        return data
    # Metodo para limpiar el texto del log
    def clearLog(self):
        self.pteLog.clear()

    # Metodo para añdir texto al log
    def appendLog(self, text):

        text_aux = ""

        text_aux = self.pteLog.toPlainText()
        text_aux = text_aux + "\n" + text
        self.pteLog.setPlainText(text_aux)

    # Metodo para mostrar dialogo emergente info
    def showInfo(self, msg):
        msgBox = QMessageBox()
        msgBox.setIcon(QMessageBox.Information)
        msgBox.setText(msg)
        msgBox.setWindowTitle("INFO")
        msgBox.exec_()

    # Metodo para mostrar dialogo emergente error
    def showError(self, msg):
        msgBox = QMessageBox()
        msgBox.setIcon(QMessageBox.Critical)
        msgBox.setText(msg)
        msgBox.setWindowTitle("ERROR")
        msgBox.exec_()

    def showWarning(self, msg):
        msgBox = QMessageBox()
        msgBox.setIcon(QMessageBox.Warning)
        msgBox.setText(msg)
        msgBox.setWindowTitle("WARNING")
        msgBox.exec_()

    # Validador de datos antes de ejecutar
    def validateData(self):

        msg_error = ""

        if self.cbLocation.currentIndex()<0 :
            msg_error = msg_error + "Seleccione la ubicacion\n"

        if self.cbConnectionServer.isEnabled() and self.cbConnectionServer.currentIndex() <= 0:
            msg_error = msg_error + "Seleccione la conexion con el sevidor\n"

        if self.cbTypeLoad.currentIndex()<0:
            msg_error = msg_error + "Seleccione el tipo de carga\n"

        if self.cbSubTypeLoad.isEnabled() and self.cbSubTypeLoad.currentIndex() < 0:
            msg_error = msg_error + "Seleccione el subtipo de carga\n"

        if self.lePathFile == None or not self.lePathFile.text().strip():
            msg_error = msg_error + "Introduzca la ruta del fichero a cargar\n"

        if self.cbConnectionTarget.currentIndex() <= 0 :
            msg_error = msg_error + "Seleccione una conexion para el destino\n"

        if self.cbSchemaTarget.currentIndex() <= 0 :
            msg_error = msg_error + "Seleccione un esquema para el destino\n"

        if self.cbObjectTarget.currentIndex() <= 0 :
            msg_error = msg_error + "Seleccione un objeto para el destino\n"

        if self.leJobName.text() == None or not self.leJobName.text():
            msg_error = str(msg_error) + "Introduzca un nombre para el job\n"

        if self.leDelimiter.text() == None or not self.leDelimiter.text():
            msg_error = msg_error + "Introduzca un delimitador\n"

        if self.leNumSessions.text() == None or not self.leNumSessions.text():
            msg_error = msg_error + "Introduzca el numero de sessiones\n"

        if self.leNumInstances.text() == None or not self.leNumInstances.text():
            msg_error = msg_error + "Introduzca el numero de instancias\n"

        if self.checkStatementPersonalized.isChecked():
            if self._data_select_statement == None:
                msg_error = msg_error + "La sentencia personalizada se encuentra vacia\n"

        if msg_error == "":
            return True
        else:
            self.showWarning(msg_error)
            return False


    # Activar/Desactivar todos los componentes
    def setEnabledComponents(self, flag):
        self.lePathTpt.setEnabled(flag)

        self.cbSchemaTarget.setEnabled(flag)

        self.cbObjectTarget.setEnabled(flag)

        self.cbConnectionTarget.setEnabled(flag)

        self.btReset.setEnabled(flag)
        self.btCreate.setEnabled(flag)

