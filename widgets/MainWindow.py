from ui.MainUi import *

from widgets.ExportWindow import ExportWindow
from widgets.LoadWindow import LoadWindow

from actions.Actions import Actions

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

    _actions = None

    def __init__(self, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        self.setupUi(self)
        self.initComponents()

    def initComponents(self):
        self._actions = Actions(self)

        self.connectSignals()

    #Connexion de slots
    def connectSignals(self):
        self.btExport.clicked.connect(self.actionExport)
        self.btLoad.clicked.connect(self.actionLoad)

    #Acciones
    def actionExport(self):
        dialog = ExportWindow()
        dialog.exec_()

    def actionLoad(self):
        dialog = LoadWindow()
        dialog.exec_()

















